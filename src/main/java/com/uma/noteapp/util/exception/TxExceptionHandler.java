package com.uma.noteapp.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.uma.noteapp.response.Response;



/**
 * <p>
 *        Exception Handler to handle custom exception at global level.
 *
 * @author Uma Shankar
 * @since 13-Aug-2018 <br>
 *        
 */
@ControllerAdvice
public class TxExceptionHandler {

	Logger logger = LoggerFactory.getLogger(TxExceptionHandler.class);

	@ExceptionHandler(TxException.class)
	public ResponseEntity<Response> noteServiceExceptionHandler(TxException txException) {
		logger.info(txException.getMessage());
		return new ResponseEntity<Response>(txException.getErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response> runTimeExceptionHandler(Exception exception) {
		exception.printStackTrace();
		TxException ex = new TxException(101, new Object[] { " " + exception.getMessage() }, exception);
		logger.error(ex.getLogMessage());
		return new ResponseEntity<Response>(ex.getErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}