package com.uma.noteapp.util.modelmapping;

/**
 * <p>
 * Generic Interface having method which is used to convert value object to DTo
 * and vice-versa.
 * </p>
 * 
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 * 
 * 
 */
public interface IGenericModelConverter {

	/**
	 * @param entity
	 * @param dto
	 * @return <br>
	 *         <p>
	 *         <b>This Method is used to convert the Value Object or Entity Object
	 *         to DTO Object, DTO Object will pass as Response for the UI.</b>
	 */
	public <T, E> T convertToDTO(E entity, Class<T> dto);

	/**
	 * @param dto
	 * @param entity
	 * @return <br>
	 *         <p>
	 *         Method to Convert DTO object to Value object or Entity Object with
	 *         the Help of Object Mapper.
	 */
	public <T, E> T convertToEntity(E dto, Class<T> entity);

}