package com.uma.noteapp.util.modelmapping;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Generic Class having method to convert Data Transfer Object to Entity and vice-versa.
 * @author Uma Shankar
 * @since 13-Aug-2018 <br>
 *        
 */
@Service
public class GenericConverterImpl implements IGenericModelConverter {

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public <T, E> T convertToDTO(E entity, Class<T> dto) {
		T dt = modelMapper.map(entity, dto);
		return dt;
	}

	@Override
	public <T, E> T convertToEntity(E dto, Class<T> entity) {
		T enti= modelMapper.map(dto, entity);
		return enti;
	}

}
