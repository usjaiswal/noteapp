package com.uma.noteapp.util;

import com.uma.noteapp.util.exception.ResourceNotFoundException;
import com.uma.noteapp.util.exception.TxException;

public class RestPreconditions {
    public static <T> T checkFound(T resource) throws TxException {
        if (resource == null) {
            throw new TxException(110);
        }
        return resource;
    }
    
    /**
	 * Ensures that an object reference passed as a parameter to the calling method is not null.
	 * @param reference an object reference
	 * @return the non-null reference that was validated
	 * @throws ResourceNotFoundException if {@code reference} is null
	 */
	public static < T >T checkNotNull( final T reference ){
		if( reference == null ){
			throw new ResourceNotFoundException();
		}
		return reference;
	}
	
	public static < T,E >T checkNotNull( final T reference,long code ) throws TxException{
		if( reference == null || reference == ""){
			throw new TxException(code);
		}
		return reference;
	}
}
