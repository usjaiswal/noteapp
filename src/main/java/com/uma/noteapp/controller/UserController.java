package com.uma.noteapp.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;
import com.uma.noteapp.entity.dto.UserDTO;
import com.uma.noteapp.response.Response;
import com.uma.noteapp.service.IUserService;
import com.uma.noteapp.util.RestPreconditions;
import com.uma.noteapp.util.exception.TxException;

import io.swagger.annotations.ApiOperation;

import static com.uma.noteapp.util.security.TokenService.HEADER_STRING;
import static com.uma.noteapp.util.security.TokenService.TOKEN_PREFIX;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Controller to demonstrate what the REST API would use to get things
 *        done.
 * 
 *        </p>
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService iUService;

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "Add User", response = Response.class)
	@PostMapping("/signup")
	public ResponseEntity<?> addUser(@RequestBody UserDTO userDTO) throws Exception {
		LOGGER.info("INSIDE Add Company");
		Response response = new Response();
		Preconditions.checkNotNull(userDTO);
		String id = RestPreconditions.checkNotNull(iUService.saveUser(userDTO));
		response.setResponseMessage("User with " + id + " added succesfully");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@ApiOperation(value = "User Login", response = Response.class)
	@PostMapping(value = "/login")
	public ResponseEntity<?> login(@RequestBody UserDTO userDTO, HttpServletResponse res) throws TxException {
		String token = RestPreconditions.checkNotNull(iUService.loginUser(userDTO), 151);

		Response response = new Response();
		res.setHeader(HEADER_STRING, TOKEN_PREFIX + token);
		response.setResponseMessage(TOKEN_PREFIX + token);
		return new ResponseEntity<Response>(response, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Fetch User By Id", response = Response.class)
	@GetMapping(value = "/getuser/{id}")
	public ResponseEntity<?> getUserById(@PathVariable("id") String id,
			@RequestHeader("Authorization") String bearerToken) throws TxException {
		UserDTO userDTO = iUService.getUserbyId(id);
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.FOUND);
	}
}
