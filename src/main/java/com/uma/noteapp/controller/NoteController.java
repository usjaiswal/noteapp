package com.uma.noteapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uma.noteapp.entity.Note;
import com.uma.noteapp.entity.dto.NoteDTO;
import com.uma.noteapp.response.Response;
import com.uma.noteapp.service.INoteService;
import com.uma.noteapp.util.exception.TxException;

import io.swagger.annotations.ApiOperation;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Controller to demonstrate what the REST API would use to get things
 *        done. NoteController consist of API related to Note operations.
 *        </p>
 */
@RestController
@RequestMapping("/note")
public class NoteController {

	@Autowired
	private INoteService iNoteService;

	private final Logger logger = LoggerFactory.getLogger(NoteController.class);

	@ApiOperation(value = "Create Note", response = Response.class)
	@RequestMapping(value = "/addnote", method = RequestMethod.POST)
	public ResponseEntity<Response> addNote(@RequestBody NoteDTO noteDTO,
			@RequestHeader("Authorization") String bearerToken, HttpServletRequest req) throws TxException {
		logger.debug("Adding note :-", noteDTO);
		String loggedInUserId = (String) req.getAttribute("id");
		String id = iNoteService.createNote(noteDTO, loggedInUserId);

		Response response = new Response();
		response.setStatusCode(200);
		response.setResponseMessage("Note with id " + id + " Added successfully..");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "Get a Note by Id", response = Response.class)
	@RequestMapping(value = "/getnote/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getNoteById(@PathVariable("id") String id,
			@RequestHeader("Authorization") String bearerToken, HttpServletRequest req) throws TxException {

		Note note = iNoteService.getNoteById(id);
		return new ResponseEntity<>(note, HttpStatus.FOUND);
	}

	@ApiOperation(value = "Get a Note by Id", response = Response.class)
	@RequestMapping(value = "/getallnotes", method = RequestMethod.GET)
	public ResponseEntity<?> getAllNotes(@RequestHeader("Authorization") String bearerToken, HttpServletRequest req)
			throws TxException {
		String loggedInUserId = (String) req.getAttribute("id");
		List<Note> notes = iNoteService.getAllNoteByUserId(loggedInUserId);
		return new ResponseEntity<>(notes, HttpStatus.FOUND);
	}

	@ApiOperation(value = "Delete Note by Id", response = Response.class)
	@RequestMapping(value = "/deletenote", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteNoteById(@RequestHeader("Authorization") String bearerToken, HttpServletRequest req,
			@RequestParam("id") String id) throws TxException {
		String noteId = iNoteService.deleteNote(id);

		Response response = new Response();
		response.setStatusCode(200);
		response.setResponseMessage("Note with id " + noteId + " Deleted successfully..");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "Update Existing Note", response = Response.class)
	@RequestMapping(value = "/updatenote", method = RequestMethod.PUT)
	public ResponseEntity<?> updateNote(@RequestHeader("Authorization") String bearerToken, HttpServletRequest req,
			@RequestBody NoteDTO noteDTO) throws TxException {
		String noteId = iNoteService.updateNote(noteDTO);
		Response response = new Response();
		response.setStatusCode(200);
		response.setResponseMessage("Note with id " + noteId + " Updated successfully..");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
