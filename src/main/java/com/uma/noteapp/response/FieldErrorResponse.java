package com.uma.noteapp.response;

import java.util.List;

import org.springframework.validation.FieldError;
/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 * 
 */
public class FieldErrorResponse extends Response {

	private List<FieldError> errors;

	   public List<FieldError> getErrors()
	   {
	      return errors;
	   }

	   public void setErrors(List<FieldError> errors)
	   {
	      this.errors = errors;
	   }

	   @Override
	   public String toString()
	   {
	      return "UserFieldErrors [errors=" + errors + "]";
	   }
}