package com.uma.noteapp.response;

/**
 * <p>
 * <b>Custom Response Class having response status code and respective Message.</b>
 * </p>
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 * 
 */
public class Response {
	
	 private long statusCode;
	 private String responseMessage;
	 
	public long getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(long statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
