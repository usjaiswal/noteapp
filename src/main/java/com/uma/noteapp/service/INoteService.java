package com.uma.noteapp.service;


import java.util.List;

import com.uma.noteapp.entity.Note;
import com.uma.noteapp.entity.dto.NoteDTO;
import com.uma.noteapp.util.exception.TxException;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Interface for Note Service to create abstraction layer of 
 *        NoteService Which will expose to Controller.
 *        </p>
 */
public interface INoteService {
	
	/**
	 * This method is used to create Note by taking NoteDTO as in parameter.
	 * @param noteDTO
	 * @param loggedInUserId
	 * @return String
	 * @throws TxException
	 */
	public String createNote(NoteDTO noteDTO, String loggedInUserId) throws TxException;
	
	/**
	 * This method is called to update existing Note.
	 * @param noteDTO
	 * @return String 
	 * @throws TxException
	 */
	public String updateNote(NoteDTO noteDTO) throws TxException;
	
	/**
	 * To get a particular Note corresponding to given Note id.
	 * @param id
	 * @return Note Object
	 * @throws TxException
	 */
	public Note getNoteById(String id) throws TxException;
	
	/**
	 * This method is used to remove the Existing Note by Id.
	 * @param id
	 * @return String
	 */
	public String deleteNote(String id);
	
	/**
	 * To Fetch All Note corresponding to the loggedIn user id.
	 * @param userId
	 * @return List<Note>
	 * @throws TxException
	 */
	public List<Note> getAllNoteByUserId(String userId) throws TxException;

}
