package com.uma.noteapp.service;

import com.uma.noteapp.entity.dto.UserDTO;
import com.uma.noteapp.util.exception.TxException;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Interface for User Service to create abstraction layer of 
 *        UserService Which will expose to Controller.
 *        </p>
 */
public interface IUserService {
	
	/**
	 * This method is used to register a particular User by providing user Info.
	 * @param userDTO
	 * @return String
	 * @throws TxException
	 */
	public String saveUser(UserDTO userDTO) throws TxException;
	
	/**
	 * To Authenticate a user by their Email and password.If Email and password matches
	 * it will generate a Token corresponding to that user.
	 * @param userDTO
	 * @return String
	 * @throws TxException
	 */
	public String loginUser(UserDTO userDTO) throws TxException;
	
	/**
	 * This method is used to Fetch a User by given user id.
	 * @param id
	 * @return UserDTO Object
	 * @throws TxException
	 */
	public UserDTO getUserbyId(String id) throws TxException;

}
