package com.uma.noteapp.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.uma.noteapp.entity.User;
import com.uma.noteapp.entity.dto.UserDTO;
import com.uma.noteapp.persistence.IUserPersistence;
import com.uma.noteapp.service.IUserService;
import com.uma.noteapp.util.RestPreconditions;
import com.uma.noteapp.util.exception.TxException;
import com.uma.noteapp.util.modelmapping.IGenericModelConverter;
import com.uma.noteapp.util.security.TokenService;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *      
 */
@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserPersistence iUserPersistence;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private IGenericModelConverter genericModelConverter;

	@Override
	public String saveUser(UserDTO userDTO) throws TxException {
		RestPreconditions.checkNotNull(userDTO.getName(), 100);
		RestPreconditions.checkNotNull(userDTO.getEmail(), 101);
		RestPreconditions.checkNotNull(userDTO.getContactnumber(), 102);
		RestPreconditions.checkNotNull(userDTO.getPassword(), 103);
		if (iUserPersistence.findByEmail(userDTO.getEmail()) != null) {
			throw new TxException(203);
		}
		User user = genericModelConverter.convertToEntity(userDTO, User.class);
		user = iUserPersistence.save(user);
		return user.getId();

	}

	@Override
	public String loginUser(UserDTO userDTO) throws TxException {
		String token = null;
		RestPreconditions.checkNotNull(userDTO.getEmail(), 101);
		RestPreconditions.checkNotNull(userDTO.getPassword(), 103);
		Query query = new Query();
		query.fields().include("password");
		query.addCriteria(Criteria.where("email").is(userDTO.getEmail())
				.andOperator(Criteria.where("password").is(userDTO.getPassword())));
		User user = mongoTemplate.findOne(query, User.class);
		RestPreconditions.checkNotNull(user, 151);
		System.out.println(user.getId());
		token =TokenService.generateToken(user.getId());
		return user != null ? token : "Not found.";
	}

	@Override
	public UserDTO getUserbyId(String id) throws TxException {

		RestPreconditions.checkNotNull(id, 105);
		User user = iUserPersistence.findById(id).get();
		genericModelConverter.convertToDTO(user, UserDTO.class);
		return genericModelConverter.convertToDTO(user, UserDTO.class);
	}

}
