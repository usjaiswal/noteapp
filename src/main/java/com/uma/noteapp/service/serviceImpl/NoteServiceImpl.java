package com.uma.noteapp.service.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uma.noteapp.entity.Note;
import com.uma.noteapp.entity.dto.NoteDTO;
import com.uma.noteapp.persistence.INotePersistence;
import com.uma.noteapp.service.INoteService;
import com.uma.noteapp.util.RestPreconditions;
import com.uma.noteapp.util.exception.TxException;
import com.uma.noteapp.util.modelmapping.IGenericModelConverter;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *       
 */
@Service
public class NoteServiceImpl implements INoteService{
	
	@Autowired
	private INotePersistence iNotePersistence;
	
	@Autowired
	private IGenericModelConverter genericModelConverter;

	@Override
	public String createNote(NoteDTO noteDTO, String loggedInUserId) throws TxException {
		RestPreconditions.checkNotNull(noteDTO.getTitle(), 110);
		RestPreconditions.checkNotNull(noteDTO.getDescription(), 111);
		Note note = genericModelConverter.convertToEntity(noteDTO, Note.class);
		note.setUserId(loggedInUserId);
		note.setCreationTime(new Date());
		note.setUpdateTime(new Date());
		note = iNotePersistence.save(note);
		return note.getId();
	}

	@Override
	public String updateNote(NoteDTO noteDTO) throws TxException {
		Note note = iNotePersistence.findById(noteDTO.getId()).get();
		RestPreconditions.checkNotNull(note, 105);
        note.update(noteDTO.getTitle(), noteDTO.getDescription());
        note = iNotePersistence.save(note);
		return note.getId();
	}

	@Override
	public Note getNoteById(String id) throws TxException {
		Note note = iNotePersistence.findById(id).get();
		RestPreconditions.checkNotNull(note, 105);
		return note;
	}

	@Override
	public String deleteNote(String id) {
		iNotePersistence.deleteById(id);
		return id;
	}

	@Override
	public List<Note> getAllNoteByUserId(String userId) throws TxException {
		List<Note> notes = iNotePersistence.findAll();
		notes = notes.stream().filter(x-> x.getUserId().equalsIgnoreCase(userId))
				.collect(Collectors.toList());
		RestPreconditions.checkNotNull(notes, 105);
		return notes;
	}

}
