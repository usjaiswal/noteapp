package com.uma.noteapp.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.uma.noteapp.entity.Note;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Persistence API which extends MongoRepository to perform DB Operation
 *        on Business Entity.
 * 
 *        </p>
 */
@Repository
public interface INotePersistence extends MongoRepository<Note, String> {

}



