package com.uma.noteapp.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.uma.noteapp.entity.User;

/**
 * @author UmaShankar
 * @since 13-Aug-2018 <br>
 *        <blockquote>
 *        <p>
 *        Persistence API which extends MongoRepository to perform DB Operation
 *        on User business Entity.
 * 
 *        </p>
 */
@Repository
public interface IUserPersistence extends MongoRepository<User, String> {
	
	@Query("{ 'email' : ?0 }")
	public User findByEmail(String email);

}
