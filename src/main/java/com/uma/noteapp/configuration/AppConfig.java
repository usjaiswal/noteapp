package com.uma.noteapp.configuration;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Configuration class which consist of Different config related to application.
 * @author UmaShankar
 *
 */
@Configuration
public class AppConfig {

	private static MessageSourceAccessor messageSourceAccessor;

	@PostConstruct
	private void initMessageSourceAccessor() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages/errormessage");
		messageSourceAccessor = new MessageSourceAccessor(messageSource, Locale.getDefault());
	}

	public static MessageSourceAccessor getMessageAccessor() {
		return messageSourceAccessor;
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
}
