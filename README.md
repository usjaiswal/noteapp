## Note Application
This Application is Developed using **Spring Boot and MongoDB.** Note App is a note taking application by which a user can Add Note by providing Title and 
Description. 

User can rgister himself by providing User information like Name,Email,ContactNumber,etc. User Authentication is done by using Email and Password and 
an Authenticated user can access the secure API using Token which was generated at the time of successful Login.

---

** Prerequisite**

1. MongoDB install on System

2. Clone the Project **git clone https://bitbucket.org/usjaiswal/noteapp**

